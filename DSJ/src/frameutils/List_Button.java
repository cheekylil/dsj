package frameutils;

import javax.swing.JButton;

/**
 * This is a SubClass of JButton.This class inherits everything from JButton. In
 * addition to that ,it has fields for storing the details of a person
 */
public class List_Button extends JButton {

	private static final long serialVersionUID = 1L;
	public boolean male;
	public String father, mother, name, spouse;
	public String sib;
	public String kid;

	public List_Button(String a) {
		super(a);
		// TODO Auto-generated constructor stub
	}

	public List_Button() {
		super();
	}

}
