package graph;

import frames.OptionsMenu;

/**
 * This Class is used to add new members to the GRAPH class
 * 
 */

public class Add {

	Graph g;

	public Add() {
		g = new Graph();
	}

	public static void main(String args[]) {
		Add l = new Add();
		/**
		 * order: name gender mother father spouse sibling children
		 */
		l.add("bob-mom", false, "bob-mom-mom", "bob-mom-dad", "bob-dad",
				new String[] { "bob-mom-bro", "bob-mom-sis" },
				new String[] { "bob" });
		l.add("bob", true, "bob-mom", "bob-dad", "bob-wife", new String[] {
				"bob-bro", "bob-sis" }, new String[] { "bob-kid1", "bob-kid2" });
		l.add("bob-dad", true, "bob-dad-mom", "bob-dad-dad", "bob-mom",
				new String[] { "bob-dad-bro", "bob-dad-sis" },
				new String[] { "bob" });

		l.output();

	}

	/**
	 * The add function is used to add a person and the relationships into the
	 * graph
	 * 
	 * @param name
	 *            : name of the person filling in the details
	 * @param male
	 *            : boolean true if male, false if female
	 * @param mother
	 *            : name of the mother
	 * @param father
	 *            : name of father
	 * @param spouse
	 *            : name of spouse
	 * @param siblings
	 *            : array of siblings
	 * @param children
	 *            : array of children simply enter null if any of the fields are
	 *            to be blank Adding a relation from P1 -> P2 also adds a
	 *            relation from P2 -> P1 (addEdge() function)
	 */
	public void add(String name, boolean male, String mother, String father,
			String spouse, String siblings[], String children[]) {
		// Add him to the graph
		g.addVertex(name);
		// Add his direct relations
		if (mother != null)
			g.addEdge(name, mother, "mother-child");
		if (father != null)
			g.addEdge(name, father, "father-child");
		if (spouse != null)
			g.addEdge(name, spouse, "spouse-spouse");
		if (siblings != null)
			for (String sib : siblings) {
				g.addEdge(name, sib, "sibling-sibling");
				if (mother != null)
					g.addEdge(sib, mother, "mother-child");
				if (father != null)
					g.addEdge(sib, father, "father-child");
			}

		if (children != null)
			for (String s : children)
				g.addEdge(s, name, male ? "father-child" : "mother-child");
		// Add the indirect relations
		if (spouse != null && children != null)
			for (String s : children)
				g.addEdge(s, spouse, male ? "mother-child" : "father-child");
		if (mother != null && father != null)
			g.addEdge(mother, father, "spouse-spouse");
		if (children != null) {
			for (int i = 0; i < (children.length - 1); i++) {
				for (int j = (i + 1); j < children.length; j++) {
					g.addEdge(children[i], children[j], "sibling-sibling");
				}
			}
		}
		if (siblings != null) {
			for (int i = 0; i < (siblings.length - 1); i++) {
				for (int j = (i + 1); j < siblings.length; j++) {
					g.addEdge(siblings[i], siblings[j], "sibling-sibling");
				}
			}
		}

	}

	/*
	 * Temporarily in place to test output.
	 */
	public void output() {

		@SuppressWarnings("unused")
		OptionsMenu w = new OptionsMenu(g);

	}
}