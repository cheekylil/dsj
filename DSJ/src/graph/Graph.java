package graph;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.TreeSet;

import plotutils.ShortestPath;

public class Graph {
	/**
	 * This class is the FAMILY TREE This holds the Graph of the MainMenu.This
	 * has all the methods for adding or removing or finding vertices . This
	 * also has BFS method which is used for Shortest path This contains the
	 * adjacency lists for all the vertices too
	 */

	// from name to vertex, using HashMap
	private HashMap<String, Vertex> myVertices;

	// from edge to relationship, using HashMap
	private HashMap<String, String> relation;

	// Adjacent list is a set of vertices represented by a search tree:
	// TreeSet<Vertex>

	// from vertex to adjacent list, using HashMap
	private HashMap<Vertex, TreeSet<Vertex>> myAdjList;

	private int myNumVertices;
	private int myNumEdges;

	// EMPTY_SET is a constant:
	private static final TreeSet<Vertex> EMPTY_SET = new TreeSet<Vertex>();
	public static final int INFINITY = Integer.MAX_VALUE;

	// Construct an empty Graph
	public Graph() {
		myAdjList = new HashMap<Vertex, TreeSet<Vertex>>();
		myVertices = new HashMap<String, Vertex>();
		relation = new HashMap<String, String>();
		myNumVertices = myNumEdges = 0;
	}

	/**
	 * Add a new vertex to the graph (if the vertex does not already exist.) If
	 * the vertex already exists, it returns the vertex
	 * 
	 * @param name
	 *            - key to refer to the vertex
	 * @return the vertex that was added to the graph
	 */
	public Vertex addVertex(String name) {
		if (name == null)
			return null;
		Vertex v;
		v = myVertices.get(name);
		if (v == null) {
			v = new Vertex(name);
			myVertices.put(name, v);
			myAdjList.put(v, new TreeSet<Vertex>());
			myNumVertices += 1;
		}
		return v;
	}

	/**
	 * gives the relation between ANY two nodes on the graph, even if not
	 * adjacent
	 * 
	 * @param s
	 *            source vertex
	 * @param d
	 *            destination vertex
	 */
	public void getFarRelation(Vertex s, Vertex d) {
		this.initSearch();
		Vertex temp = null;
		Queue<Vertex> q = new LinkedList<Vertex>();
		s.predecessor = null;
		s.discovered = true;
		q.add(s);
		while (!q.isEmpty()) {
			temp = q.remove();
			if (temp.equals(d))
				break;
			else
				for (Vertex w : this.adjacentTo(temp)) {
					if (w.discovered)
						continue;
					w.predecessor = temp;
					q.add(w);
					w.discovered = true;
				}
		}
		List<Vertex> l = new LinkedList<Vertex>();
		while (temp != s) {
			l.add(temp);

			temp = temp.predecessor;
		}
		l.add(s);
		Collections.reverse(l);

		String st = "";
		for (int i = 0; i < l.size(); i++) {
			st = st + l.get(i);
			st = st + " ";
		}

		StringBuilder sb = new StringBuilder();
		if (d.name.equals(s.name))
			sb.append(d.name + " is only " + s.name);
		else
			sb.append(d.name + " is " + s.name + "'s ");

		for (int i = 0; i < l.size() - 1; ++i) {

			sb.append(i != l.size() - 2 ? (getRelation(l.get(i).name,
					l.get(i + 1).name) + "'s ") : getRelation(l.get(i).name,
					l.get(i + 1).name));
		}

		ShortestPath.drawrelation(st, sb.toString());

	}

	/**
	 * Returns the Vertex matching v
	 * 
	 * @param name
	 *            a String name of a Vertex that may be in this Graph
	 * @return the Vertex with a name that matches v or null if no such Vertex
	 *         exists in this Graph
	 */
	public Vertex getVertex(String name) {
		return myVertices.get(name);
	}

	/**
	 * Returns true iff v is in this Graph, false otherwise
	 * 
	 * @param name
	 *            a String name of a Vertex that may be in this Graph
	 * @return true iff v is in this Graph
	 */
	public boolean hasVertex(String name) {
		return myVertices.containsKey(name);
	}

	/**
	 * Is from-to, an edge in this Graph. The graph is undirected so the order
	 * of from and to does not matter.
	 * 
	 * @param from
	 *            the name of the first Vertex
	 * @param to
	 *            the name of the second Vertex
	 * @return true iff from-to exists in this Graph
	 */
	public boolean hasEdge(String from, String to) {
		Vertex v1 = myVertices.get(from);
		Vertex v2 = myVertices.get(to);
		if (v1 == null || v2 == null)
			return false;
		return myAdjList.get(v1).contains(v2);
	}

	/**
	 * Returns the relation between two adjacent (vertices) on the graph.
	 * 
	 * @param from
	 *            - Name of person from
	 * @param to
	 *            - Name of person two
	 * @return the relation between "from" and "to"
	 */
	public String getRelation(String from, String to) {
		String rkey = from + ";" + to;
		return (relation.get(rkey));
	}

	/**
	 * Add to to from's set of neighbors, and add from to to's set of neighbors.
	 * Does not add an edge if another edge already exists
	 * 
	 * @param from
	 *            the name of the first Vertex
	 * @param to
	 *            the name of the second Vertex
	 */
	public void addEdge(String from, String to, String rel) {
		if (from == null || to == null)
			return;
		if (from.equalsIgnoreCase(to))
			return;
		// We need to add two relations, from N1 -> N2 and also from N2 -> N1
		String a[] = rel.split("-");
		// rkey is the unique key to identify a relation.
		String rkey1 = from + ";" + to;
		String rkey2 = to + ";" + from;
		// Add both relations to the HashMap
		relation.put(rkey1, a[0]);
		relation.put(rkey2, a[1]);
		Vertex v, w;
		if (hasEdge(from, to))
			return; // no duplicate edges
		myNumEdges += 1;
		if ((v = getVertex(from)) == null)
			v = addVertex(from);
		if ((w = getVertex(to)) == null)
			w = addVertex(to);
		myAdjList.get(v).add(w);
		myAdjList.get(w).add(v); // undirected graph only
	}

	/**
	 * Return an iterator over the neighbors of Vertex named v
	 * 
	 * @param name
	 *            the String name of a Vertex
	 * @return an Iterator over Vertices that are adjacent to the Vertex named
	 *         v, empty set if v is not in graph
	 */
	public Iterable<Vertex> adjacentTo(String name) {
		Vertex v = getVertex(name);
		if (v == null)
			return EMPTY_SET;
		return myAdjList.get(getVertex(name));
	}

	/**
	 * Return an iterator over the neighbors of Vertex v
	 * 
	 * @param v
	 *            the Vertex
	 * @return an Iterator over Vertices that are adjacent to the Vertex v,
	 *         empty set if v is not in graph
	 */
	public Iterable<Vertex> adjacentTo(Vertex v) {
		if (!myAdjList.containsKey(v))
			return EMPTY_SET;
		return myAdjList.get(v);
	}

	/**
	 * Returns an Iterator over all Vertices in this Graph
	 * 
	 * @return an Iterator over all Vertices in this Graph
	 */
	public Iterable<Vertex> getVertices() {
		return myVertices.values();
	}

	/**
	 * @return the number of vertices in the graph
	 */
	public int numVertices() {
		return myNumVertices;
	}

	/**
	 * @return the number of edges in the graph
	 */
	public int numEdges() {
		return myNumEdges;
	}

	/*
	 * Returns adjacency-list representation of graph
	 */
	public String toString() {
		String s = "";
		for (Vertex v : myVertices.values()) {
			s += v + ": ";
			for (Vertex w : myAdjList.get(v)) {
				s += w + " ";
			}
			s += "\n";
		}
		return s;
	}

	/**
	 * Called before a search function. Resets values of vertices for the
	 * search.
	 */
	private void initSearch() {
		for (Vertex v : this.getVertices()) {
			v.processed = v.discovered = false;
			v.predecessor = null;
			v.distance = INFINITY;
			v.height = 0;
		}
	}

	/**
	 * heightSet function sets the height.
	 * 
	 * @param s
	 *            - source vertex taken as height 0. heights of other vertices
	 *            set in relation to source.
	 */

	public void heightSet(Vertex s) {
		this.initSearch();
		recHeight(s, 0);
		int minHeight = s.height;

		for (Vertex w : getVertices()) {
			if (w.height < minHeight)
				minHeight = w.height;

		}
		for (Vertex w : getVertices()) {

			w.height += (-1 * minHeight);
		}
		for (@SuppressWarnings("unused")
		Vertex w : getVertices()) {

		}

	}

	/**
	 * 
	 * @param s
	 *            is the source vertex
	 * @param height
	 *            height of source vertex recHeight is the recursive
	 *            implementation used for heightSet
	 * 
	 */

	public void recHeight(Vertex s, int height) {
		if (!s.discovered) {
			s.height = height;
			s.discovered = true;
			for (Vertex w : this.adjacentTo(s.name)) {
				if (getRelation(w.name, s.name).equals("mother")
						|| getRelation(w.name, s.name).equals("father"))
					recHeight(w, height - 1);
				if (getRelation(w.name, s.name).equals("sibling")
						|| getRelation(w.name, s.name).equals("spouse"))
					recHeight(w, height);
				else
					recHeight(w, height + 1);
			}
		}
	}

}