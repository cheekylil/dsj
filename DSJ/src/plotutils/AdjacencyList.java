package plotutils;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.ScrollPane;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * This Class has a static method used to plot the adjacency list of all Nodes
 * Adjacency list plots all nodes adjacent to a node
 */
public class AdjacencyList {

	private static JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					AdjacencyList.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AdjacencyList() {
		initialize();
	}

	static JPanel pa = new JPanel();
	static ScrollPane scrollPane_2;

	/**
	 * This gives the adjacency plot for all nodes.Input is a total string array
	 * (t) where each element is an adjacency list
	 * 
	 * @param t
	 */
	public static void drawgraph(String[] t) {
		frame = new JFrame("Adjacency Plot For All Nodes");
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		pa.removeAll();
		frame.revalidate();
		frame.repaint();
		for (int i = 0; i < t.length; i++) {
			JPanel label = new JPanel(new FlowLayout(FlowLayout.LEFT));
			String test = t[i].replace(":", "");
			String s[] = test.split(" ");
			JButton head = new JButton(s[0]);
			head.setBackground(Color.LIGHT_GRAY);
			label.add(head);
			JLabel arrow = new JLabel("\u2192");
			arrow.setFont(arrow.getFont().deriveFont(32.0f));
			label.add(arrow);
			for (int j = 1; j < s.length; j++) {
				JButton butt = new JButton(s[j]);
				butt.setBackground(Color.white);
				label.add(butt);
				if (j != s.length - 1) {
					JLabel arroww = new JLabel("\u2192");
					arroww.setFont(arroww.getFont().deriveFont(32.0f));
					label.add(arroww);
				}

			}
			pa.add(label);
		}

		scrollPane_2 = new ScrollPane();
		scrollPane_2.setBounds(97, 160, 327, 330);
		frame.getContentPane().add(scrollPane_2);
		pa.setLayout(new BoxLayout(pa, BoxLayout.Y_AXIS));
		scrollPane_2.add(pa);
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

	}

}
