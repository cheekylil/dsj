package plotutils;

import java.awt.Color;

import javax.swing.JFrame;

/**
 * This class is used along with the DirectRelations class to show visual
 * representation of immediate relations
 * 
 *
 */
public class Helper {
	/**
	 * This method takes the following parameters and produces a JFrame window
	 * with the immediate relations graph
	 * 
	 * @param name
	 * @param father
	 * @param mother
	 * @param spouse
	 * @param siblings
	 * @param kids
	 */
	public static void printim(String name, String father, String mother,
			String spouse, String siblings, String kids) {
		DirectRelations panel = new DirectRelations("Direct Relations Of "
				+ name);
		JFrame frame = new JFrame("Direct Relations Of " + name);
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frame.add(panel);

		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBackground(Color.WHITE);
		frame.setSize(750, 500);

		frame.setVisible(true);
		if (father.equals(""))
			father = name + "'s" + " father. (Unspecified)";
		panel.addNode(father, 125, 100);
		if (mother.equals(""))
			mother = name + "'s" + " mother. (Unspecified)";
		panel.addNode(mother, 520, 100);
		String sib[] = siblings.split(";");
		int namelen = sib.length / 2;
		String temp = sib[sib.length - 1];
		for (int j = sib.length; j < namelen; j--) {
			sib[j + 1] = sib[j];
		}
		sib[namelen] = name;
		String kid[] = kids.split(";");
		int length = sib.length;
		int difference = 500 / length;
		int x = 100;
		int midx = 200;
		for (int i = 0; i < length; i++) {
			int p = 200;
			if (i == length / 2) {
				p = 250;
				midx = x;
			}
			panel.addNode(sib[i], x, p);
			x = x + difference;
			if (!father.equals(""))
				panel.addEdge(0, i + 2);
			if (!mother.equals(""))
				panel.addEdge(1, i + 2);
		}
		int p = sib.length + 2;
		if (temp != "") {
			panel.addNode(temp, x, 200);
			panel.addEdge(0, p);
			panel.addEdge(1, p);
			p++;
		}
		if (!spouse.equals("")) {

			panel.addNode(spouse, midx + 150, 250);
			panel.linecolor = Color.BLACK;
			panel.addEdge(namelen + 2, p);
			if (!kid[0].equals("")) {
				length = kid.length;

				difference = 750 / (length);

				x = difference / 2;
				int y = 350;
				panel.linecolor = Color.BLACK;
				for (int i = 0; i < length; i++) {
					panel.addNode(kid[i], x, y);
					panel.addEdge(namelen + 2, p + 1 + i);
					panel.addEdge(p, p + 1 + i);
					x = x + difference;
				}
			}
			}
		
		panel.addEdge(0, 2);
		panel.addEdge(1, 2);
		panel.addEdge(0, 1);

	}

}