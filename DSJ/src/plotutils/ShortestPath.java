package plotutils;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.ScrollPane;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * This Class has a static method which is used to visually show relations
 * between any two nodes on the graph This basically shows a visual
 * representation of the shortest path obtained by BFS
 */

public class ShortestPath {
	private static JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					ShortestPath.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ShortestPath() {
		initialize();
	}

	static JPanel pa = new JPanel();
	static ScrollPane scrollPane_2;

	/**
	 * This method uses the following parameters to show the relation link
	 * between any two members of the family
	 * 
	 * @param t
	 * @param relation
	 */
	public static void drawrelation(String t, String relation) {
		frame = new JFrame(relation);
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		pa.removeAll();
		frame.revalidate();
		frame.repaint();
		JPanel label = new JPanel();
		String test = t.replace(":", "");

		String s[] = test.split(" ");
		JButton head = new JButton(s[0]);
		head.setBackground(Color.LIGHT_GRAY);
		label.add(head);
		JLabel arrow = new JLabel("\u2192");
		arrow.setFont(arrow.getFont().deriveFont(32.0f));
		if (s.length != 1)
			label.add(arrow);
		for (int j = 1; j < s.length; j++) {
			if (j == s.length - 1) {
				JButton butt = new JButton(s[j]);
				butt.setBackground(Color.LIGHT_GRAY);
				label.add(butt);
			} else {
				JButton butts = new JButton(s[j]);
				butts.setBackground(Color.WHITE);
				label.add(butts);
			}
			if (j != s.length - 1) {
				JLabel arroww = new JLabel("\u2192");
				arroww.setFont(arroww.getFont().deriveFont(32.0f));
				label.add(arroww);
			}

		}
		pa.add(label);

		scrollPane_2 = new ScrollPane();
		scrollPane_2.setBounds(97, 160, 327, 330);
		frame.getContentPane().add(scrollPane_2);
		pa.setLayout(new BoxLayout(pa, BoxLayout.Y_AXIS));
		scrollPane_2.add(pa);
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

	}

}
