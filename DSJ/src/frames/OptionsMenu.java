package frames;

import graph.Graph;
import graph.Vertex;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import plotutils.AdjacencyList;
import plotutils.Helper;

/**
 * This Class contains options for the different features of the mini-project.
 * It has Options to a)Show adjacency plots b)Get relationship between any two
 * members(USING BFS) c)Show all immediate relations for any member
 * 
 */
public class OptionsMenu {

	private JFrame frame;
	private Graph g;
	private JTextField source;
	private JTextField dest;
	private JTextField imname;
	private JButton printim;
	JLabel errorlabel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					OptionsMenu window = new OptionsMenu();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public OptionsMenu() {
		initialize();
	}

	/**
	 * 
	 * @param g
	 *            Takes in the graph as a parameter and initializes it
	 */
	public OptionsMenu(Graph g) {
		initialize();
		this.g = g;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Select your choice");
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setVisible(true);
		JButton adjplotbtn = new JButton("Show plot for all Nodes");
		adjplotbtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				errorlabel.setText("");

				AdjacencyList.drawgraph(g.toString().split("\n"));

			}
		});
		adjplotbtn.setBounds(0, 11, 434, 23);
		frame.getContentPane().add(adjplotbtn);

		source = new JTextField();
		source.setBounds(0, 45, 147, 20);
		frame.getContentPane().add(source);
		source.setColumns(10);

		dest = new JTextField();
		dest.setColumns(10);
		dest.setBounds(170, 45, 147, 20);
		frame.getContentPane().add(dest);

		JButton btnGetRelation = new JButton("Get Relation");
		btnGetRelation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				getRelation();
			}
		});
		btnGetRelation.setBounds(327, 44, 107, 23);
		frame.getContentPane().add(btnGetRelation);

		imname = new JTextField();
		imname.setBounds(0, 110, 147, 20);
		frame.getContentPane().add(imname);
		imname.setColumns(10);

		printim = new JButton("Show Immediate Relations");
		printim.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				printim();
			}
		});
		printim.setBounds(170, 109, 264, 23);
		frame.getContentPane().add(printim);

		errorlabel = new JLabel("");
		errorlabel.setBounds(10, 236, 414, 14);
		frame.getContentPane().add(errorlabel);
	}

	/**
	 * Method used for displaying all the immediate relations for any given node
	 * 
	 */
	public void printim() {
		errorlabel.setText("");
		String name = imname.getText();
		try {
			Vertex w = g.getVertex(name);

			String father = "", mother = "", spouse = "";
			StringBuilder sib = new StringBuilder();

			String siblings = "";
			String kids = "";
			for (Vertex v : g.adjacentTo(w)) {

				if (g.getRelation(w.name, v.name).equals("father"))
					father = v.name;
				if (g.getRelation(w.name, v.name).equals("mother"))
					mother = v.name;
				if (g.getRelation(w.name, v.name).equals("spouse"))
					spouse = v.name;
				if (g.getRelation(w.name, v.name).equals("sibling"))
					sib.append(v.name).append(";");
				if (g.getRelation(w.name, v.name).equals("child"))
					kids = kids + v.name + ";";
			}

			if (sib.length() != 0)
				siblings = sib.toString();

			Helper.printim(w.name, father, mother, spouse, siblings, kids);
		} catch (Exception e) {

			errorlabel.setText("Something went wrong!");

		}
	}

	/**
	 * Method which gets and displays the relation between the two input members
	 * given by the user
	 */
	public void getRelation() {
		try {
			errorlabel.setText("");
			String s = source.getText();
			String d = dest.getText();
			if (g.hasVertex(s) || g.hasVertex(d))
				g.getFarRelation(g.getVertex(s), g.getVertex(d));

		} catch (Exception e) {
			errorlabel.setText("Something went wrong!");
		}
	}
}
