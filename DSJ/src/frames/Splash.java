package frames;

import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Splash {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Splash window = new Splash();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Splash() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Family Tree");
		frame.setBounds(100, 100, 450, 550);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel label1 = new JLabel("");
		label1.setBounds(14, 36, 410, 368);

		ImageIcon ico = new ImageIcon("src/family.jpg");
		label1.setIcon(ico);
		frame.getContentPane().add(label1);

		JLabel lblFamilyTree = new JLabel("Family Tree");
		lblFamilyTree.setBounds(156, 11, 173, 14);
		frame.getContentPane().add(lblFamilyTree);

		JButton btnBegin = new JButton("BEGIN.");
		btnBegin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] temp = new String[10];
				MainMenu.main(temp);
				frame.setVisible(false);
			}
		});
		btnBegin.setBounds(304, 431, 89, 23);
		frame.getContentPane().add(btnBegin);

		JLabel lblMiniprojectBy = new JLabel("Mini-Project by:");
		lblMiniprojectBy.setBounds(14, 415, 89, 14);
		frame.getContentPane().add(lblMiniprojectBy);

		JLabel lblAman = new JLabel("Aman");
		lblAman.setBounds(14, 440, 46, 14);
		frame.getContentPane().add(lblAman);

		JLabel lblKarthik = new JLabel("Karthik");
		lblKarthik.setBounds(14, 460, 46, 14);
		frame.getContentPane().add(lblKarthik);

		JLabel lblAkash = new JLabel("Akash");
		lblAkash.setBounds(14, 479, 46, 14);
		frame.getContentPane().add(lblAkash);
	}
}
