package frames;

import frameutils.List_Button;
import graph.Add;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * This is the Class which is used to take inputs to the FAMILY TREE from the
 * user This class is used to communicate between the user and the Graph
 * 
 */
public class MainMenu extends JFrame {

	private static final long serialVersionUID = 1L;
	private JFrame frame;
	private JTextField textField;
	private JTextField mother;
	private JTextField spouse;
	private JTextField children;
	private JTextField siblings;
	ScrollPane scrollPane_2;
	JPanel pa = new JPanel();
	JCheckBox chckbxMale;
	private LinkedList<List_Button> cards; // Using a Stack to store the cards
	int i = 0;
	private JTextField father;
	Color background;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainMenu window = new MainMenu();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainMenu() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		frame = new JFrame("WELCOME");
		frame.setBounds(100, 100, 550, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		scrollPane_2 = new ScrollPane();
		scrollPane_2.setBounds(97, 160, 327, 330);

		frame.getContentPane().setLayout(null);
		frame.setVisible(true);

		cards = new LinkedList<List_Button>();
		JButton addbtn = new JButton("ADD");
		background = addbtn.getBackground();
		addbtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				add();
				// frame.getContentPane().setBackground(Color.WHITE);
				frame.getContentPane().add(scrollPane_2);
				frame.revalidate();
				frame.repaint();
			}
		});

		addbtn.setBounds(10, 503, 257, 47);

		frame.getContentPane().add(addbtn);

		JLabel lblNewLabel = new JLabel("NAME        ");
		lblNewLabel.setBounds(0, 11, 87, 14);
		frame.getContentPane().add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("MOTHER   ");
		lblNewLabel_1.setBounds(0, 37, 87, 14);
		frame.getContentPane().add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("SPOUSE   ");
		lblNewLabel_2.setBounds(0, 88, 87, 14);
		frame.getContentPane().add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("CHILDREN ");
		lblNewLabel_3.setBounds(0, 138, 87, 14);
		frame.getContentPane().add(lblNewLabel_3);

		textField = new JTextField();
		textField.setBounds(97, 8, 327, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);

		mother = new JTextField();
		mother.setColumns(10);
		mother.setBounds(97, 34, 327, 20);
		frame.getContentPane().add(mother);

		spouse = new JTextField();
		spouse.setColumns(10);
		spouse.setBounds(97, 85, 327, 20);
		frame.getContentPane().add(spouse);

		children = new JTextField();
		children.setColumns(10);
		children.setBounds(97, 135, 327, 20);
		frame.getContentPane().add(children);

		JLabel label = new JLabel("SIBLINGS ");
		label.setBounds(0, 113, 87, 14);
		frame.getContentPane().add(label);

		siblings = new JTextField();
		siblings.setColumns(10);
		siblings.setBounds(97, 110, 327, 20);
		frame.getContentPane().add(siblings);

		JButton buildbtn = new JButton("BUILD");
		buildbtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				buildtree();

			}

		});
		buildbtn.setBounds(267, 503, 257, 47);
		frame.getContentPane().add(buildbtn);

		JLabel lblFather = new JLabel("FATHER   ");
		lblFather.setBounds(0, 62, 87, 14);
		frame.getContentPane().add(lblFather);

		father = new JTextField();
		father.setColumns(10);
		father.setBounds(97, 59, 327, 20);
		frame.getContentPane().add(father);

		chckbxMale = new JCheckBox("MALE?");
		chckbxMale.setBounds(460, 7, 64, 23);
		frame.getContentPane().add(chckbxMale);
	}

	/**
	 * This method is called when the build button is clicked. It takes the
	 * information stored on the card stack and checks for all null conditions
	 * and adds the appropriate nodes to the graph class and launches the
	 * "OptionsMenu" Frame which contains the different options
	 */
	public void buildtree() {

		Add l = new Add();
		while (!cards.isEmpty()) {

			List_Button s = cards.peek();
			boolean male = cards.peek().male;
			cards.pop();
			String t[] = s.kid.split(";");
			if (t[0].equals(""))
				t = null;
			String sib[] = s.sib.split(";");
			if (sib[0].equals(""))
				sib = null;
			if (s.mother.equals(""))
				s.mother = null;
			if (s.father.equals(""))
				s.father = null;
			if (s.name.equals(""))
				s.name = null;
			if (s.spouse.equals(""))
				s.spouse = null;
			l.add(s.name, male, s.mother, s.father, s.spouse, sib, t);

		}
		l.output();
		frame.setVisible(false);

	}

	/**
	 * This method extracts information for the Menu and adds the details to the
	 * card stack
	 */

	public void add() {
		String m = mother.getText();
		String c = children.getText();
		String n = textField.getText();
		String s = spouse.getText();
		String b = siblings.getText();
		String f = father.getText();

		List_Button label;
		label = new List_Button("<html>" + "Name:" + n + "<br>" + "Mother:" + m
				+ "<br>" + "Father:" + f + "<br>" + "Spouse:" + s + "<br>"
				+ "Siblings:" + b + "<br>" + "Children:" + c + "</html>");
		label.setBackground(Color.WHITE);
		label.mother = m;
		label.father = f;
		label.spouse = s;
		label.name = n;
		label.sib = b;
		label.kid = c;
		label.male = chckbxMale.isSelected();
		label.setContentAreaFilled(false);
		label.setOpaque(true);
		label.addActionListener(new ActionListener(
				) {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				List_Button l=(List_Button)e.getSource();
				int response=JOptionPane.showConfirmDialog(null, "Are you sure you want to delete?", "Confirm", JOptionPane.YES_NO_OPTION);
				if(response==JOptionPane.YES_OPTION)
				{
					cards.remove(l);
					pa.remove(l);
					pa.revalidate();
					pa.repaint();
				}
			}
		});




		
		
		
		
		
		cards.push(label);
		pa.add(label);
		pa.setLayout(new BoxLayout(pa, BoxLayout.Y_AXIS));
		scrollPane_2.add(pa);

	}
}
